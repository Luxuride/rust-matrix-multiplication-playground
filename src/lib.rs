#![feature(test)]
use std::{
    fmt::Display,
    ops::{Add, Mul},
};

use num::{CheckedAdd, CheckedMul};
mod bench;
mod suboptimal;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Matrix<T> {
    pub data: Vec<Vec<T>>,
}

pub trait Transpose {
    fn transpose(&self) -> Self;
}

impl<T> Transpose for Matrix<T>
where
    T: Clone,
{
    fn transpose(&self) -> Self {
        let mut transposed: Vec<Vec<T>> = Vec::new();
        for (_, row) in self.data.iter().enumerate() {
            for (j, item) in row.iter().enumerate() {
                if let None = transposed.get(j) {
                    transposed.push(vec![]);
                }
                transposed[j].push(item.clone());
            }
        }
        Matrix { data: transposed }
    }
}

impl<T> Add for Matrix<T>
where
    T: Clone,
    T: Add<T, Output = T>,
{
    type Output = Matrix<T>;

    fn add(self, rhs: Self) -> Self::Output {
        let mut res = vec![];
        for (i, row) in self.data.iter().enumerate() {
            res.push(vec![]);
            for (j, item) in row.iter().enumerate() {
                res[i].push(item.clone() + rhs.data[i][j].clone());
            }
        }
        Matrix { data: res }
    }
}

impl<T> CheckedAdd for Matrix<T>
where
    T: Clone,
    T: CheckedAdd,
{
    fn checked_add(&self, v: &Self) -> Option<Self> {
        let mut res = vec![];
        for (i, row) in self.data.iter().enumerate() {
            res.push(vec![]);
            for (j, item) in row.iter().enumerate() {
                res[i].push(item.checked_add(&v.data[i][j])?);
            }
        }
        Some(Matrix { data: res })
    }
}

impl<'a, T> Mul for &'a Matrix<T>
where
    T: Mul<T, Output = T>,
    T: Clone,
    T: Add<T, Output = T>,
{
    type Output = Matrix<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        let rhs = rhs.transpose();
        let mut res: Vec<Vec<T>> = Vec::new();
        for (i, row) in self.data.iter().enumerate() {
            res.push(vec![]);
            for (j, item) in row.iter().enumerate() {
                let mut other_iter = rhs.data[j].iter();
                let mut added: T = item.clone() * other_iter.next().unwrap().clone();
                for k in other_iter {
                    added = added + (item.clone() * k.clone());
                }
                res[i].push(added);
            }
        }
        Matrix { data: res }
    }
}

impl<T> Mul for Matrix<T>
where
    T: Mul<T, Output = T>,
    T: Clone,
    T: Add<T, Output = T>,
{
    type Output = Matrix<T>;
    fn mul(self, rhs: Self) -> Self::Output {
        (&self).mul(&rhs)
    }
}

impl<T> CheckedMul for Matrix<T>
where
    T: CheckedMul,
    T: Clone,
    T: CheckedAdd,
{
    fn checked_mul(&self, v: &Self) -> Option<Self> {
        let rhs = v.transpose();
        let mut res: Vec<Vec<T>> = Vec::new();
        for (i, row) in self.data.iter().enumerate() {
            res.push(vec![]);
            for (j, item) in row.iter().enumerate() {
                let mut other_iter = rhs.data[j].iter();
                let mut added: T = item.checked_mul(other_iter.next()?)?;
                for k in other_iter {
                    added = added.checked_add(&item.checked_mul(k)?)?;
                }
                res[i].push(added);
            }
        }
        Some(Matrix { data: res })
    }
}

impl<T> Display for Matrix<T>
where
    T: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut res: String = "".to_owned();
        for i in &self.data {
            res += "\n|";
            for j in i {
                res += "item: ";
                let value = (&j).to_string();
                let mut iter = value.split('\n').peekable();
                while let Some(row) = iter.next() {
                    res += "   ";
                    res += row;
                    if let Some(_) = iter.peek() {
                        res += "\n";
                    }
                }
                res += ", ";
            }
            res += "|"
        }
        f.write_str(&res)
    }
}
