use std::ops::{Add, AddAssign, Mul};

use crate::Matrix;

pub trait NoTranspoistionMul {
    fn nt_mul(self, rhs: Self) -> Self::Output;
    type Output;
}

impl<T> NoTranspoistionMul for Matrix<T>
where
    T: Mul<T, Output = T>,
    T: Clone,
    T: AddAssign,
    T: Add<T, Output = T>,
{   
    type Output = Matrix<T>;
    fn nt_mul(self, rhs: Self) -> Self::Output {
        let mut res: Vec<Vec<T>> = Vec::new();
        for (i, row) in self.data.iter().enumerate() {
            res.push(vec![]);
            for (j, item) in row.iter().enumerate() {
                let mut added: T = item.clone() * rhs.data[0][j].clone();
                for i in 1..rhs.data.len() {
                    added += item.clone() * rhs.data[i][j].clone();
                }
                res[i].push(added);
            }
        }
        Matrix { data: res }
    }
}
