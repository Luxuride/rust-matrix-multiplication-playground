extern crate test;
use num::CheckedMul;
use rand::{self, distributions::Standard, prelude::Distribution};
use test::Bencher;

use crate::{suboptimal::NoTranspoistionMul, Matrix};

fn _get_random_matrix<T>(rows: usize, cols: usize) -> Matrix<T>
where
    Standard: Distribution<T>,
{
    let mut matrix: Vec<Vec<T>> = vec![];
    for i in 0..rows {
        matrix.push(vec![]);
        for _ in 0..cols {
            matrix[i].push(rand::random::<T>());
        }
    }
    return Matrix { data: matrix };
}

#[test]
fn matrices_match() {
    let matrix1 = _get_random_matrix::<f64>(500, 200);
    let matrix2 = _get_random_matrix::<f64>(200, 500);
    let multiplied1 = (&matrix1) * (&matrix2);
    let multiplied2 = matrix1.nt_mul(matrix2);
    assert_eq!(multiplied1, multiplied2);
}
#[test]
fn i32_matrice_mul() {
    let matrix1 = _get_random_matrix::<i32>(500, 200);
    let matrix2 = _get_random_matrix::<i32>(200, 500);
    matrix1.checked_mul(&matrix2);
}

#[bench]
fn multiplication_matrix_matrix_2_2_f64_5_5(b: &mut Bencher) {
    b.iter(|| {
        let mut matrices: Vec<Vec<Matrix<f64>>> = vec![];
        let mut matrices2: Vec<Vec<Matrix<f64>>> = vec![];
        for i in 0..2 {
            matrices.push(vec![]);
            matrices2.push(vec![]);
            for _ in 0..2 {
                matrices[i].push(_get_random_matrix::<f64>(5, 5));
                matrices2[i].push(_get_random_matrix::<f64>(5, 5));
            }
        }
        let matrices = Matrix { data: matrices };
        let matrices2 = Matrix { data: matrices2 };
        matrices * matrices2
    });
}
#[bench]
fn multiplication_matrix_matrix_2_2_f64_100_50(b: &mut Bencher) {
    b.iter(|| {
        let mut matrices: Vec<Vec<Matrix<f64>>> = vec![];
        let mut matrices2: Vec<Vec<Matrix<f64>>> = vec![];
        for i in 0..2 {
            matrices.push(vec![]);
            matrices2.push(vec![]);
            for _ in 0..2 {
                matrices[i].push(_get_random_matrix::<f64>(100, 50));
                matrices2[i].push(_get_random_matrix::<f64>(100, 50));
            }
        }
        let matrices = Matrix { data: matrices };
        let matrices2 = Matrix { data: matrices2 };
        matrices * matrices2
    });
}
#[bench]
fn multiplication_f64_5_5(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(5, 5);
        let matrix2 = _get_random_matrix::<f64>(5, 5);
        matrix1 * matrix2
    });
}
#[bench]
fn multiplication_f64_5_5_nt(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(5, 5);
        let matrix2 = _get_random_matrix::<f64>(5, 5);
        matrix1.nt_mul(matrix2)
    });
}

#[bench]
fn multiplication_f64_100_50(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(100, 50);
        let matrix2 = _get_random_matrix::<f64>(50, 100);
        matrix1 * matrix2
    });
}

#[bench]
fn multiplication_f64_100_50_nt(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(100, 50);
        let matrix2 = _get_random_matrix::<f64>(50, 100);
        matrix1.nt_mul(matrix2)
    });
}

#[bench]
fn multiplication_f64_500_500(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(500, 500);
        let matrix2 = _get_random_matrix::<f64>(500, 500);
        matrix1 * matrix2
    });
}

#[bench]
fn multiplication_f64_500_500_nt(b: &mut Bencher) {
    b.iter(|| {
        let matrix1 = _get_random_matrix::<f64>(500, 500);
        let matrix2 = _get_random_matrix::<f64>(500, 500);
        matrix1.nt_mul(matrix2)
    });
}
