# Rust Multiplication Playground
This is my playground repo for implementing, optimising and benchmarking matrix multiplication in rust.
As I find faster and faster solutions, I'll be putting the old ones into suboptimal.rs for reference benchmarks.
This will also include multi threaded solutions which will be compared differently to single threaded.

## Requirements
Rust nightly

## How to use
You can create main.rs to play with the struct, create your own project that calls it as library (**Disclaimer: This project is not production ready and will probably never be**) or you can just run `cargo bench` and `cargo test` and see the funny numbers.

## Features
Normal and nested matrix multiplication. I got the nested matrix multiplication thanks to the generics.

## Some project choices
### Why use clone instead of copy?
So I can pass Matrix as generic to the Matrix multiplication.